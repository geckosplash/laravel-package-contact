<?php

namespace GeckoSplash\Contact\Http\Controllers;

use App\Http\Controllers\Controller;
use GeckoSplash\Contact\Mail\ContactMailable;
use GeckoSplash\Contact\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class ContactController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('contact::contact');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function send(Request $request)
    {
        Mail::to(config('contact.send_email_to'))->send(new ContactMailable($request->message, $request->name));
        Contact::create($request->all());
        return redirect(route('contact'));
    }
}
