<?php

namespace GeckoSplash\Contact\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMailable extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    private $message;

    /** @var string */
    private $name;

    /**
     * ContactMailable constructor.
     * @param string $message
     */
    public function __construct(string $message, string $name)
    {
        $this->message = $message;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('contact::contact.email')->with(['message' => $this->message, 'name' => $this->name]);
    }
}
